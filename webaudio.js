let ctx
document.onclick = function(){
  if(ctx == undefined){
    ctx = new (window.AudioContext || window.webkitAudioContext)();
    console.log("context created");
  }
}

function playNote(freq, synth, panVal=0) {
  if (ctx == undefined) {
    return
  }
  // do not play if gain = 0:
  if(synth.gain != 0 || freq != "0"){ 
    // Main osc
    let osc
    if(synth.type == 'noise'){
      osc = whiteNoise(ctx, synth.gain)
    }else{
      osc = ctx.createOscillator()
      osc.frequency.value = freq;
      osc.type = synth.type
    }

    // Main gain
    const oscGain = ctx.createGain()
    oscGain.gain.value = synth.gain 
    // map() relates gain vals to oscGain
    oscGain.gain.setValueCurveAtTime(new Float32Array(synth.env).map(function(x){return x * synth.gain}), ctx.currentTime, synth.dur); 
    
    // Modulator
    const modOsc = ctx.createOscillator()
    modOsc.frequency.value = 1
    const modGain = ctx.createGain()
    modGain.gain.value = synth.tre
    modOsc.connect(modGain)
    if (synth.type != 'noise') {
      modGain.connect(osc.frequency)
    }
   
    // Panner
    let panNode = ctx.createStereoPanner();
    panNode.pan.value = panVal; 
    osc.connect(panNode);
    // panner depends on the main gain
    panNode.connect(oscGain); 
  
    // Oscillator
    // oscGain comes from panNode
    oscGain.connect(ctx.destination)
    osc.start(ctx.currentTime)
    modOsc.start(ctx.currentTime)
    osc.stop(ctx.currentTime + synth.dur);
    modOsc.stop(ctx.currentTime + synth.dur)
  }
}

// White noise

function whiteNoise(ctx, ampl){
  const whiteBuffer = ctx.createBuffer(2, ctx.sampleRate*1, ctx.sampleRate)
  for (let ch=0; ch<whiteBuffer.numberOfChannels; ch++) {
      let samples = whiteBuffer.getChannelData(ch)
      for (let s=0; s<whiteBuffer.length; s++) samples[s] = Math.random()* ampl * 2- ampl
  }
  return new AudioBufferSourceNode(ctx, {buffer:whiteBuffer})
}

function clamp(val, min, max) {
  return val > max ? max : val < min ? min : val;
}

function getEuclPattern(beats, steps) {
  // Bjorklund algorithm from https://gist.github.com/withakay/1286731
  steps = Math.round(steps)
  beats = Math.round(beats)
  if (beats > steps || beats == 0 || isNaN(beats) || steps == 0 || isNaN(steps)) {
    console.info('Element needs width / height');
    return []
  }
  let pattern = []
  let counts = []
  let remainders = []
  let divisor = steps - beats
  remainders.push(beats)
  let level = 0
  while (true) {
    counts.push(Math.floor(divisor / remainders[level]))
    remainders.push(divisor % remainders[level])
    divisor = remainders[level]
    level += 1
    if (remainders[level] <= 1) {
      break
    }
  }
  counts.push(divisor)
  let r = 0
  const build = function(level) {
    r += 1
    if (level > -1) {
      for (var i = 0; i < counts[level]; i++) {
        build(level - 1)
      }
      if (remainders[level] !== 0) {
        build(level - 2)
      }
    } else if (level === -1) {
      pattern.push(0)
    } else if (level === -2) {
      pattern.push(1)
    }
  }
  build(level)
  return pattern.reverse()
}