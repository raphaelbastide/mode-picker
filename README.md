# Mode Picker

Helps to pick a musical mode, and translate it to a list of raw frequencies.

Requires a webserver to run.

## License

GNU GPL